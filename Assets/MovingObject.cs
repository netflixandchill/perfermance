﻿using UnityEngine;
using System.Collections;

public class MovingObject : MonoBehaviour {
	 
	public float Speed = 0f;
	private float movex = 0f; 
	private float movey = 0f;

	void Start () {
		
	}

	void Update () {
		if (Input.GetKey (KeyCode.A))
			movex = -1;
		else if (Input.GetKey (KeyCode.D))
			movex = 1;
		else
			movex = 0; 
		if (Input.GetKey (KeyCode.W))
			
			movey = 1;
		else
			movey = 0;
	}
	void FixedUpdate(){
		GetComponent<Rigidbody2D>().velocity = new Vector2(movex*Speed, movey*Speed);
	}

}
