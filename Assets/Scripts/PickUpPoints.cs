﻿using UnityEngine;
using System.Collections;

public class PickUpPoints : MonoBehaviour {

	public int scoreToGive;

	private ScoreManager theScoreManager;
	void Start () {
		theScoreManager = FindObjectOfType<ScoreManager>();
	}

	void Update () {

	}
	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.gameObject.name == "Player")
		{
			theScoreManager.AddScore(scoreToGive);
			gameObject.SetActive(false);
		}
	}
}